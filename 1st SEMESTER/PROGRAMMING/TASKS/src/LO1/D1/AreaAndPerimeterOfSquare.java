public class AreaAndPerimeterOfSquare {
	public static void main(String[]args){
		int l=90; //set the variable
		
		int area=l*l; //Area of Square = length*length
		int perimeter=4*l; //perimeter of Square = 4 × (length of any one side)
		
		System.out.println("Length Of Square = "+l); //print length of Square
		System.out.println("Area of Square = "+area); // print Area of Square
		System.out.println("Perimeter of Square = "+perimeter); // print perimeter of Square
	}
}


