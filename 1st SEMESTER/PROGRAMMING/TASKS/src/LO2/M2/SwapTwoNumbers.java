package lo2.m2;
/**
* @author Niluxy
*/
 public class SwapTwoNumbers {
    /*
      *  This is the main method of this program
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int num1=60;
	int num2=20;
		System.out.println("Before Swapping");
		System.out.println("Num1: "+num1+ ","+ "Num2: "+num2);
 	num1=num1+num2;
	num2=num1-num2;
	num1=num1-num2;
		System.out.println("After Swapping");
		System.out.println("Num1: "+num1 +","+ " Num2: "+num2);
	}
}
    
    
/*
output
Before Swapping
Num1: 60,Num2: 20
After Swapping
Num1: 20, Num2: 60

*/

