package lo2.d2;

public class FindLeapYear {
	public static void main(String args[]){
		int year=1985;
	 	System.out.println(year);
		if(((year%4 ==0) && (year%100 ==0))||(year%400 ==0))
			System.out.println("IT IS A LEAP YEAR");
		else
			System.out.println("IT IS NOT A LEAP YEAR");
		}
}


/*
output
1985
IT IS NOT A LEAP YEAR
*/