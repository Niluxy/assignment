package lo4.p4;

public class Star {
	public static void main(String args[]){
		int num=10; //set the variable
		
		double J=Math.pow(num,2); 
		for (int i=1; i<=J; i++){ //loop for number of rows(j) 
			System.out.print("*"); //print *
			if(i%num==0){
				System.out.println(""); //print space
			}
		}
	}
}